using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")] //dadurch wird in Unity ein neues Menu Item "Card" generiert
public class Card2 : ScriptableObject /*Generierung ScriptableObject => Ein ScriptableObject ist ein Datencontainer, den du verwenden 
                                      kannst, um große Mengen an Daten unabhängig von Klasseninstanzen zu speichern. 
                                      Im Gegensatz zu herkömmlichen C#-Skripten müssen ScriptableObjects nicht direkt an 
                                      GameObjects angehängt werden
                                      */
{   

    //In Unity können dann Werte zugeteilt werden
    public new string name; 
    public string description;
    public int manaCost;
    public int attack;

    public void Print ()
    {
        Debug.Log(name + ": " + description + " The card costs: " + manaCost);
    }
}