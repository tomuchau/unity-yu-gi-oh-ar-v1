using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDatabase : MonoBehaviour
{
    public static List<Card> cardList = new List<Card>();

    void Awake()
    {
        cardList.Add(new Card(0, "The Winged Dragon of Ra", 3, 1, 0, "I'm gold", Resources.Load<Sprite>("Card Sprites/0 The Winged Dragon of Ra")));
        cardList.Add(new Card(1, "Slifer the Sky Dragon", 2, 2, 0, "I'm red", Resources.Load<Sprite>("Card Sprites/1 Slifer the Sky Dragon")));
        cardList.Add(new Card(2, "Obelisk the Tormentor", 1, 3, 0, "I'm blue", Resources.Load<Sprite>("Card Sprites/2 Obelisk the Tormentor")));
    }
    
}