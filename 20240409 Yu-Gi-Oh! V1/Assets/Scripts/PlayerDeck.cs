using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeck : MonoBehaviour
{
    // For randomly filled initial Player Deck 
    public List<Card> container = new List<Card>();
    // For Card Shuffling initialized Player Deck
    public int x;
    public static int deckSize;
    public List<Card> deck = new List<Card>();
    public static List<Card> staticDeck = new List<Card>();

    // Deck Display
    public GameObject cardInDeck1;
    public GameObject cardInDeck2;
    public GameObject cardInDeck3;
    public GameObject cardInDeck4;
    public GameObject cardInDeck5;
    public GameObject cardInDeck6;
    public GameObject cardInDeck7;
    public GameObject cardInDeck8;
    public GameObject cardInDeck9;
    public GameObject cardInDeck10;

    public GameObject CardToHand;
    public GameObject[] Clones;
    public GameObject Hand;

    // Start is called before the first frame update
    void Start()
    {
        x = 0;
        deckSize = 10;

        for (int i = 0; i < deckSize; i++)
        {
            // Interval corresponds to card ID range
            x = Random.Range(0, CardDatabase.cardList.Count);
            deck[i] = CardDatabase.cardList[x];
        }

        StartCoroutine(StartGame());
    }

    // Update is called once per frame
    void Update()
    {
        staticDeck = deck;
        if (deckSize < 10)
        {
            cardInDeck10.SetActive(false);
        }
        if (deckSize < 9)
        {
            cardInDeck9.SetActive(false);
        }
        if (deckSize < 8)
        {
            cardInDeck8.SetActive(false);
        }
        if (deckSize < 7)
        {
            cardInDeck7.SetActive(false);
        }
        if (deckSize < 6)
        {
            cardInDeck6.SetActive(false);
        }
        if (deckSize < 5)
        {
            cardInDeck5.SetActive(false);
        }
        if (deckSize < 4)
        {
            cardInDeck4.SetActive(false);
        }
        if (deckSize < 3)
        {
            cardInDeck3.SetActive(false);
        }
        if (deckSize < 2)
        {
            cardInDeck2.SetActive(false);
        }
        if (deckSize < 1)
        {
            cardInDeck1.SetActive(false);
        }

        //Draws a card at start of your turn
        if (TurnSystem.isYourTurnStart && deckSize > 0)
        {
            StartCoroutine(Draw(1));
            TurnSystem.isYourTurnStart = false;
        }
        else if (TurnSystem.isYourTurnStart && deckSize == 0)
        {
            TurnSystem.isYourTurnStart = false;
        }
    }

    IEnumerator StartGame()
    {
        for (int i = 0; i <= 2; i++)
        {
            yield return new WaitForSeconds(0.4f);
            Instantiate(CardToHand, transform.position, transform.rotation);
            deckSize--;
        }
    }

    // Card Shuffle Method
    public void Shuffle()
    {
        for (int i = 0; i < deckSize; i++)
        {
            // Add a new element to container
            container[0] = deck[i];
            int randomIndex = Random.Range(i, deckSize);
            deck[i] = deck[randomIndex];
            deck[randomIndex] = container[0];

        }
    }

    IEnumerator Draw(int x)
    {
        if (deckSize > 0)
        {
            for (int i = 0; i < x; i++)
            {
                yield return new WaitForSeconds(0.4f);
                Instantiate(CardToHand, transform.position, transform.rotation);
                deckSize--;
            }
        }
        else
        {
            yield break;
        }
    }
}