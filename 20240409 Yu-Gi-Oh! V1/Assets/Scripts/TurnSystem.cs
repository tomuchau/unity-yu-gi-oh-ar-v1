using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TurnSystem : MonoBehaviour
{
    public bool isYourTurn;
    public int yourTurn;
    public int opponentTurn;
    public TextMeshProUGUI buttonText;
    public Button endTurnButton;

    public int maxStars;
    public int currentStars;
    public TextMeshProUGUI starsText;

    public static bool isYourTurnStart;

    public Color yourTurnColor = Color.blue;
    public Color opponentTurnColor = Color.red;

    // Start is called before the first frame update
    void Start()
    {
        isYourTurn = true;
        yourTurn = 1;
        opponentTurn = 0;

        maxStars = 1;
        currentStars = 1;

        UpdateButtonColor();

        isYourTurnStart = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isYourTurn)
        {
            buttonText.text = "End Your Turn";
        }
        else
        {
            buttonText.text = "End Opponent Turn";
        }

        starsText.text = currentStars + "/" + maxStars;
    }

    public void EndYourTurn()
    {
        isYourTurn = false;
        opponentTurn += 1;
    }

  public void EndOpponentTurn()
{
    isYourTurn = true;
    yourTurn += 1;

    if (maxStars < 5) {
        maxStars += 1;
    }

    currentStars = maxStars;

    isYourTurnStart = true;
}

    public void EndTurn()
    {
        if (isYourTurn)
        {
            EndYourTurn();
        }
        else
        {
            EndOpponentTurn();
        }
        UpdateButtonColor();
    }

    public void UpdateButtonColor()
    {
        ColorBlock colorBlock = endTurnButton.colors;

        if (isYourTurn)
        {
            colorBlock.normalColor = yourTurnColor;
            colorBlock.highlightedColor = yourTurnColor;
            colorBlock.pressedColor = yourTurnColor;
            colorBlock.selectedColor = yourTurnColor;
        }
        else
        {
            colorBlock.normalColor = opponentTurnColor;
            colorBlock.highlightedColor = opponentTurnColor;
            colorBlock.pressedColor = opponentTurnColor;
            colorBlock.selectedColor = opponentTurnColor;
        }
        endTurnButton.colors = colorBlock;
    }
}
