using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OpponentHP : MonoBehaviour
{
    public float maxHP = 3;
    public float hp;
    public TextMeshProUGUI hpText;

    // Start is called before the first frame update
    void Start()
    {
        hp = maxHP;
    }

    // Update is called once per frame
    void Update()
    {
        hpText.text = hp.ToString("F0");
    }
}