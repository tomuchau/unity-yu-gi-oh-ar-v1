using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class DisplayCard : MonoBehaviour
{
    //Card Attributes
    public List<Card> displayCard = new List<Card>();
    public int displayId;
    public int id;
    public string cardName;
    public int cost;
    public int atk;
    public int def;
    public string cardDescription;
    public Sprite spriteImage;

    //UI Responders
    public TextMeshProUGUI cardNameText;
    public TextMeshProUGUI costText;
    public TextMeshProUGUI atkText;
    public TextMeshProUGUI defText;
    public TextMeshProUGUI cardDescriptionText;
    public Image artImage;

    //Card Back
    public bool cardBack;
    public static bool staticCardBack;
    public GameObject Hand;
    public int numberOfCardsInDeck;
   


    // Start is called before the first frame update
    void Start()
    {
        numberOfCardsInDeck = PlayerDeck.deckSize;

        displayCard[0] = CardDatabase.cardList[displayId];
        
    }

    // Update is called once per frame
    void Update()
    {
        id = displayCard[0].id;
        cardName = displayCard[0].cardName;
        cost = displayCard[0].cost;
        atk = displayCard[0].atk;
        def = displayCard[0].def;
        cardDescription = displayCard[0].cardDescription;
        spriteImage = displayCard[0].spriteImage;

        cardNameText.text = " " + cardName;
        costText.text = " " + cost;
        atkText.text = " " + atk;
        defText.text = " " + def;
        cardDescriptionText.text = " " + cardDescription;
        artImage.sprite = spriteImage;
        
        Hand = GameObject.Find("Hand");
        if(this.transform.parent == Hand.transform.parent)
        {
            cardBack = false;
        }
        //Checking Card Back Status
        staticCardBack = cardBack;

        if(this.tag == "Clone" && numberOfCardsInDeck > 0)
        {
            displayCard[0] = PlayerDeck.staticDeck[numberOfCardsInDeck - 1];
            numberOfCardsInDeck -=1;
            cardBack = false;
            this.tag = "Untagged";
        }
    }
}
