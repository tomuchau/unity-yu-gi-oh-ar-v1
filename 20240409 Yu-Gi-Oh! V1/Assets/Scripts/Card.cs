using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class Card
{
    public int id;
    public string cardName;
    public int cost;
    public int atk;
    public int def;
    public string cardDescription;
    public Sprite spriteImage;

    public Card()
    {

    }

    //Card Constructor
    public Card(int Id, string CardName, int Cost, int Atk, int Def, string CardDescription, Sprite SpriteImage)
    {
        id = Id;
        cardName = CardName;
        cost = Cost;
        atk = Atk;
        def = Def;
        cardDescription = CardDescription;
        spriteImage = SpriteImage;
    }

}
